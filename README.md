# Impart ETH Testnet

Impart Ethereum Testnet is a public permissioned Ethereum network with sic nodes distributed over the globe.
It is an alternativ to the existing testnetwork from Ethereum. Impart supports and monitor this network. We supports the user by tests with smart contracts and help them to find problems.
The network is based on parity nodes and uses the Proof of Autority (POA) consensus protokoll.

The network monitor has following URL:
http://78.141.206.6:3000

For support contact:
[testnet@dlt-consulting.ch](mailto:testnet@dlt-consulting.ch)

enodes
------
The main enode adresses are:
```
enode://7fbf955501bdf7b4ccaae0f1488e3f1152048f42969fa5b2978cba35f36f70b39ef9158542b54daffe060932fffeb2791af6415213a40e5e808a99fcc7566341@142.93.170.66:30300
enode://d993912e2e3c34d3ced067d8c75cfbe92a2a6dcdc203a868520b726ac27ee8c7a6aa75da369146e314166a6239c2c0627f393fa0547b5920a14a2d14ae540c25@209.97.163.129:30300
enode://7d117c6580ce60fd7b59899b41d158d1e103e4278564793444be8c5e0b80c06778763eb0a5ad5cd897846e7554024511008b38e195f94bfb7dbf87339197cc5d@138.197.168.164:30300
enode://d89e6470b9837cd263037081c97ce896b322badb9f0f5b2022321a0a9f3f429d7ca072bf87ec7dca02b9a5cf5863661f097f8dccda7737b992dcfef601329710@78.141.206.6:30300
enode://029300e4e71d3a312741c8323a19563652bca7cb52308039c252c62792f8c7114fbba9f7211945a1e50dccf9b463e926108c957b8d1a3f4e86669b1fed9acf75@45.76.65.52:30300
```

how to connect
------
1 get the latest parity docker image:
```
docker pull parity/parity:stable
```
2 create some where a directory for the genesis file, config file and blockchain data
```
mkdir imparttestnet
cd imparttestnet/
```
3 copy the genesis and config file to this directory
```
git clone https://gitlab.com/impartCH/impart_eth_testnet.git
```
4 edit the node.toml config file
```
vi node.toml
change this line =>
    chain = "{path to spec file}/imp-test-spec.json"
```
5 start container
```
docker run --name=parity_node -dti --restart unless-stopped -p 8540:8540 -p 8450:8450 -p 8180:8180 -p 30300:30300 -p 30300:30300/udp -v {path to created directory}:/home/parity/.local/share/io.parity.ethereum/  parity/parity:stable --base-path /home/parity/.local/share/io.parity.ethereum/node --config /home/parity/.local/share/io.parity.ethereum/node.toml --jsonrpc-interface all
```
Transactions
------
You can only made transaction with **wei**. The gas price is 10 wei.
Send your wallet address to [testnet@dlt-consulting.ch](mailto:testnet@dlt-consulting.ch) with subject "imparttestnet" and you will reseve some wei for about 100'000 transactions free to play around.
If you need more wei pleas contact us:

[www.dlt-consulting.ch](https://www.dlt-consulting.ch/#/)

Terms of Use
------
We continuously change and optimize our network and services. For example, we can add or remove functions or features and also suspend or permanently discontinue the network or a service.

We provide our network and services in an economically appropriate manner but ther are restrictions.

We make no commitments regarding the content of the network and services, the specific functionalities of the network and the services or their reliability, availability or suitability of the network and the services for your purposes.

In some legal systems, certain warranties apply, such as the implicit guarantee of fitness for purpose, suitability for a specific purpose and freedom from defects. To the extent permitted by law, we exclude all warranties.
